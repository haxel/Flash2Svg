##Unsupported features
- Motion guidess
- Filter tweens
- Drop Shadow filter
- Bevel filter
- Gradient bevel filter
- Gradient Glow filter
- Primative Shape support (rect, circle, etc)
- Detect rectangle paths and use primative shape
- Button symbols export with mouseenter and mouseleave
- Text inputs (as html foreignObject)

##Enhancements
- When opening flash by double-clicking FLA, error alerts are shown
- transforms on infinitely looping animated nodes aren't needed
- Images should be referenced like symbols (this will improve the embedding system)
- Restrict character entry on custom frame fields
- Add smil2css post-process